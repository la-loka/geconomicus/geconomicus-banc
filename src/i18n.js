import Vue from 'vue';
import VueI18n from 'vue-i18n';
Vue.use(VueI18n);

const en = require('./translations/en.js');
const es = require('./translations/es.js');
const ca = require('./translations/ca.js');
const fr = require('./translations/fr.js');
const eu = require('./translations/eu.js');

const messages = {
  en: en.language,
  es: es.language,
  ca: ca.language,
  fr: fr.language,
  eu: eu.language,
}

const english = 'en';
export const i18n = new VueI18n({
  locale: english,
  fallbackLocale: english,
  messages
});
