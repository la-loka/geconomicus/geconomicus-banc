

export const language = {
  "bank": "banc",
  "Start the game": "Començar el joc",
  "Round": "Ronda",
  "Winnings": "Guanys",
  "Losses": "Pèrdues",
  "Profit": "Benefici",
  "Players": "Jugadors/es",
  "Add name": "Afegir nom",
  "Appointments": "Cites",
  "Bank data": "Dades bancàries",
  "Is not in debt": "No te deute",
  "Has borrowed a total of": "Ha prestat un total de",
  "and written off": "i amortitzat",
  "Owes": "Owes",
  "and must pay": "i ha de pagar",
  "Write off": "Amortitza",
  "New loan": "Nou préstec",
  "Has payed": "Ha pagat",
  "in interest": "en interessos",
  "Has payed interest": "Ha pagat interessos",
  "The bank is owed": "Deu al banc",
  "Collected": "Recaptat",

  "Finish appointment": "Finalitzar la cita",
  "Update": "Actualitzar",

  "Stop the clock": "Parar el rellotge",

  "About": "About",
  "Licenses": "Licències",
}
