

export const language = {
  "Round": "Round",
  "Winnings": "Winnings",
  "Losses": "Losses",
  "Profit": "Profit",
  "Players": "Players",
  "Add name": "Add name",
  "Appointments": "Appointments",
  "Bank data": "Bank data",
  "Is not in debt": "Is not in debt",
  "Has borrowed a total of": "Has borrowed a total of",
  "and written off": "and written off",
  "Owes": "Owes",
  "and must pay": "and must pay",
  "Write off": "Write off",
  "New loan": "New loan",
  "Has payed": "Has payed",
  "in interest": "in interest",
  "Has payed interest": "Has payed interest",
  "The bank is owed": "The bank is owed",
  "Collected": "Collected",

  "Finish appointment": "Finish appointment",
  "Update": "Update",

  "Stop the clock": "Stop the clock",

  "About": "About",
  "Licenses": "Licenses",
}
