

export const language = {
  "bank": "banco",
  "Start the game": "Comenzar el juego",
  "Round": "Ronda",
  "Winnings": "Ganancias",
  "Losses": "Pérdidas",
  "Profit": "Beneficio",
  "Players": "Jugadores/as",
  "Add name": "Añadir nombre",
  "Appointments": "Citas",
  "Bank data": "Datos bancarios",
  "Is not in debt": "No tiene deuda",
  "Has borrowed a total of": "Ha prestado un total de",
  "and written off": "y amortizado",
  "Owes": "Debe",
  "and must pay": "y tiene que pagar",
  "Write off": "Amortiza",
  "New loan": "Nuevo préstamo",
  "Has payed": "Ha pagado",
  "in interest": "en intereses",
  "Has payed interest": "Ha pagado interés",
  "The bank is owed": "Debe al banco",
  "Collected": "Recaudado",

  "Finish appointment": "Finalizar la cita",
  "Update": "Actualizar",

  "Stop the clock": "Parar el reloj",

  "About": "About",
  "Licenses": "Licencias",
}
