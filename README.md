# geconomicus

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
or
npm run electron:serve
```
Debian does not open the electron window
```
sudo sysctl kernel.unprivileged_userns_clone=1
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
